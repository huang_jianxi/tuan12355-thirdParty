import Vue from 'vue'
import VueRouter from 'vue-router'
import Index from '../layout/Index.vue'
import Welcome from '../views/Welcome/Welcome.vue'
import MemberList from '../views/MemberList/MemberList.vue'
import Login from '../views/Login/Login.vue'
import LearningStatistics from '../views/LearningStatistics/LearningStatistics.vue'
import GroupFee from '../views/GroupFee/GroupFee.vue'
import RolloutSituation from '../views/RolloutSituation/RolloutSituation.vue'
import Message from '../views/Message/Message.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/admin',
        name: 'Index',
        component: Index,
        children: [
            {
                path: '/admin/status',
                component: Welcome
            },
            {
                path: '/admin/memberlist',
                component: MemberList
            },
            {
                path: '/admin/learningstatistics',
                component: LearningStatistics
            },
            {
                path: '/admin/groupfee',
                component: GroupFee
            },
            {
                path: '/admin/rolloutsituation',
                component: RolloutSituation
            },
            {
                path: '/admin/message',
                component: Message
            }
        ]
    },
    {
        path: '/',
        name: 'Login',
        component: Login
    }
]

const router = new VueRouter({
    mode: 'hash',
    base: process.env.BASE_URL,
    routes
})

export default router
