import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        SlideCollapse: false,
        token: '',
        cookie: '',
        oid: '',
        fullName: '',
        userInfo: {
            id: 1,
            username: 'admin233'
        }
    },
    mutations: {
        setSlideCollapse(state, SlideCollapse) {
            state.SlideCollapse = SlideCollapse
        },
        setToken(state, token) {
            state.token = token
        },
        setCookie(state, cookie) {
            state.cookie = cookie
        },
        setOid(state, oid) {
            state.oid = oid
        },
        setUserInfo(state, userInfo) {
            state.userInfo = userInfo
        },
        setFullName(state, fullName) {
            state.fullName = fullName
        }
    },
    actions: {

    },
    modules: {
    }
})
