const mail = {
    sendMail(password, to, title, content) {
        return {
            method: 'get',
            url: '/sendMail',
            params: {
                password: password,
                to: to,
                title: title,
                content: content
            }
        }
    }
}

export default mail
