/**
 * @typedef {Object} SubMenuOption - 子菜单选项配置
 * @property {String} name - 选项标签名
 * @property {String} index - 该项被点击后导航到的路由地址
 */

/**
 * @typedef {Object} SlideMenuOption - 侧边栏菜单配置
 * @property {String} icon - elementui图标
 * @property {String} name - 选项标签名
 * @property {'el-menu-item' | 'el-submenu'} type - 侧边栏选项类型，菜单选项或是子菜单
 * @property {String} index - 该项被点击后导航到的路由地址
 * @property {SubMenuOption[]=} children - 子菜单选项配置
 */

/**
 * @typedef {Object} WebSiteSetting - 网站模板配置
 * @property {String} title - 网站标题
 * @property {String} welecome_msg - 网站欢迎信息
 * @property {String} header_bg_color - 网页header背景颜色
 * @property {String} header_text_color - 网页header文本颜色
 * @property {String} slide_bg_color - 侧边栏背景颜色
 * @property {String} slide_text_color - 侧边栏文本颜色颜色
 * @property {String} slide_select_color - 侧边栏选中状态颜色
 *
 * @property {SlideMenuOption[]} slide_selects - 侧边栏菜单配置
 */

/**
 * @type {WebSiteSetting}
 */
const webSiteSetting = {
    title: 'DIYXI-智慧团建',
    welecome_msg: '',
    header_bg_color: '#db4254',
    header_text_color: '',
    slide_bg_color: '#f4f4f4',
    slide_text_color: 'black',
    slide_select_color: '#db4254',
    slide_selects: [
        {
            icon: 'el-icon-menu',
            name: '学习状态',
            type: 'el-menu-item',
            index: '/admin/status'
        },
        {
            icon: 'el-icon-user',
            name: '我的团员',
            type: 'el-menu-item',
            index: '/admin/memberlist'
        },
        {
            icon: 'el-icon-data-analysis',
            name: '党史学习',
            type: 'el-menu-item',
            index: '/admin/learningstatistics'
        },
        {
            icon: 'el-icon-money',
            name: '团费查询',
            type: 'el-menu-item',
            index: '/admin/groupfee'
        },
        {
            icon: 'el-icon-money',
            name: '转出情况',
            type: 'el-menu-item',
            index: '/admin/rolloutsituation'
        },
        {
            icon: 'el-icon-money',
            name: '系统消息',
            type: 'el-menu-item',
            index: '/admin/message'
        }
    ]
}

export default {
    webSiteSetting// 网站设置
}
