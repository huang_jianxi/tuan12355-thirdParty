import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element.js'
import './css/base.css'
import global from '@/config/global.js'
Vue.config.productionTip = false
Vue.prototype.$global = global
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
